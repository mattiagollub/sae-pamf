SAE Project 2: Program Analysis for Missile Firing 

Developed for the Software Architecture and Engineering class,
PAMF is a static numerical analyzer for a safety-critical missile
battery API. The Java software relies on Soot and APRON
for the structural and numerical analysis. It analyzes the code and
detects scenarios where security constraints are possibly violated.