public class UTestl1 {
    public static void l1() // UNSAFE
    {
        MissileBattery r = new MissileBattery(6);

        int i = 20;
        if (i < 42)
            i = 9;
        else
            i = 4;

        r.fire(i);
    }
}
