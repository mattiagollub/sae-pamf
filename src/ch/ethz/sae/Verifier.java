package ch.ethz.sae;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Vector;

import apron.ApronException;
import apron.Interval;
import soot.Unit;

import soot.jimple.*;
import soot.jimple.internal.JAssignStmt;
import soot.jimple.internal.JNewExpr;
import soot.jimple.internal.JimpleLocal;
import soot.jimple.spark.sets.DoublePointsToSet;
import soot.jimple.spark.sets.P2SetVisitor;
import soot.jimple.spark.SparkTransformer;
import soot.jimple.spark.pag.Node;
import soot.jimple.spark.pag.PAG;
import soot.Local;
import soot.Scene;
import soot.SootClass;
import soot.SootMethod;
import soot.Value;
import soot.toolkits.graph.BriefUnitGraph;

public class Verifier {

    private static HashMap<String, HashSet<Integer>> allocatedRes = new HashMap<String, HashSet<Integer>>();

    public static void main(String[] args) throws ApronException {
        if (args.length != 1) {
            System.err
                    .println("Usage: java -classpath soot-2.5.0.jar:./bin ch.ethz.sae.Verifier <class to test>");
            System.exit(-1);
        }
        String analyzedClass = args[0];
        SootClass c = loadClass(analyzedClass);

        PAG pointsToAnalysis = doPointsToAnalysis(c);

        int programCorrectFlag = 1;

        for (SootMethod method : c.getMethods()) {

            Analysis.dbg(method.getName());
            Analysis analysis = new Analysis(new BriefUnitGraph(
                    method.retrieveActiveBody()), c);
            Analysis.dbg(method.retrieveActiveBody());
            analysis.run();

            if (!checkMethod(method, analysis)) {
                programCorrectFlag = 0;
            }
        }
        if (programCorrectFlag == 1) {
            System.out.println("Program " + analyzedClass + " is SAFE");
        } else {
            System.out.println("Program " + analyzedClass + " is UNSAFE");
        }
    }

    private static boolean checkMethod(SootMethod method, Analysis fixPoint)
            throws ApronException
    {

        for (Unit u : method.retrieveActiveBody().getUnits()) {
            AWrapper state = fixPoint.getFlowBefore(u);
            VirtualInvokeExpr VIExpr = null;
            SpecialInvokeExpr SIExpr = null;

            if (state.get().isBottom(fixPoint.man))
                continue;

            if (u instanceof InvokeStmt
                    && ((InvokeStmt) u).getInvokeExpr() instanceof SpecialInvokeExpr) {
                SIExpr = (SpecialInvokeExpr) ((InvokeStmt) u).getInvokeExpr();
            } else if (u instanceof InvokeStmt
                    && ((InvokeStmt) u).getInvokeExpr() instanceof VirtualInvokeExpr) {
                VIExpr = (VirtualInvokeExpr) ((InvokeStmt) u).getInvokeExpr();
            }

            if (SIExpr != null && SIExpr.getMethod().getName().equals("<init>") && SIExpr.getArgs().size() != 0) { //size of the missile battery

                HashSet<Integer> hashSet = new HashSet<Integer>();
                if (SIExpr.getArg(0) instanceof JimpleLocal) {

                    Interval adj = state.get().getBound(Analysis.man, SIExpr.getArg(0).toString());

                    int max = Integer.parseInt(adj.sup.toString());
                    if ((adj.sup.isInfty() == 1) || (max > 100)) max = 100;
                    for (int i = 0; i < max; i++) {
                        hashSet.add(i);
                    }
                }
                else
                {
                    int n = ((IntConstant) SIExpr.getArg(0)).value;


                    for (int i = 0; i < n; i++) {
                        hashSet.add(i);
                    }
                }

                String nameMB = SIExpr.getBase().toString();
                for (Unit u1 : method.retrieveActiveBody().getUnits()) {
                    AWrapper state1 = fixPoint.getFlowBefore(u);
                    if (state1.get().isBottom(fixPoint.man))
                        continue;

                    if (u1 instanceof JAssignStmt) {
                        if (((JAssignStmt) u1).getRightOp().toString().equals(nameMB))
                            nameMB = ((JAssignStmt) u1).getLeftOp().toString();
                    }
                }

                allocatedRes.put(nameMB, hashSet);
                Analysis.dbg("allocatedRes = " + allocatedRes);


            } else if (VIExpr != null && VIExpr.getMethod().getName().equals("fire")) {

                Analysis.dbg("args = " + VIExpr.getArgs());

                if (VIExpr.getArg(0) instanceof JimpleLocal) {

                    Interval adj = state.get().getBound(Analysis.man, VIExpr.getArg(0).toString());

                    if ((adj.inf.isInfty() != 0) || (adj.sup.isInfty() != 0)) {
                        return false;
                    }

                    if (allocatedRes.containsKey(VIExpr.getBase().toString())) {
                        HashSet<Integer> hashSet = allocatedRes.get(VIExpr.getBase().toString());


                        int min = Integer.parseInt(adj.inf.toString());
                        int max = Integer.parseInt(adj.sup.toString());

                        for (int i = min; i <= max; i++) {
                            if (!hashSet.contains(i)) {
                                return false;
                            }
                            hashSet.remove(i);
                        }
                    }
                }
                else if (VIExpr.getArg(0) instanceof IntConstant) {
                    int n = ((IntConstant) VIExpr.getArg(0)).value;

                    if (allocatedRes.containsKey(VIExpr.getBase().toString())) {
                        HashSet<Integer> hashSet = allocatedRes.get(VIExpr.getBase().toString());

                        if (!hashSet.contains(n)) {
                            return false;
                        }
                        hashSet.remove(n);
                    }
                }
            }

        }

        return true;
    }

    private static SootClass loadClass(String name) {
        SootClass c = Scene.v().loadClassAndSupport(name);
        c.setApplicationClass();
        return c;
    }

    private static PAG doPointsToAnalysis(SootClass c) {
        Scene.v().setEntryPoints(c.getMethods());

        HashMap<String, String> options = new HashMap<String, String>();
        options.put("enabled", "true");
        options.put("verbose", "false");
        options.put("propagator", "worklist");
        options.put("simple-edges-bidirectional", "false");
        options.put("on-fly-cg", "true");
        options.put("set-impl", "double");
        options.put("double-set-old", "hybrid");
        options.put("double-set-new", "hybrid");

        SparkTransformer.v().transform("", options);
        PAG pag = (PAG) Scene.v().getPointsToAnalysis();

        return pag;
    }
}
