package ch.ethz.sae;

import java.util.*;

import apron.*;

import soot.IntegerType;
import soot.Local;
import soot.SootClass;

import soot.Unit;
import soot.Value;

import soot.jimple.*;

import soot.jimple.internal.*;

import soot.jimple.toolkits.annotation.logic.Loop;
import soot.toolkits.graph.LoopNestTree;
import soot.toolkits.graph.UnitGraph;
import soot.toolkits.scalar.ForwardBranchedFlowAnalysis;
import soot.util.Chain;

// Implement your numerical analysis here.
public class Analysis extends ForwardBranchedFlowAnalysis<AWrapper> {

    private static boolean dbg = false;
    public static Manager man;
    private Environment env;
    public UnitGraph g;
    public String local_ints[]; // integer local variables of the method
    public static String reals[] = { "foo" };
    public SootClass jclass;
    private List<String> ifSafeTypes = Arrays.asList("byte", "short", "int");
    private String className = "MissileBattery";
    private static int wideningLimit = 5;
    private HashMap<Unit, Integer> wideningCounts = new HashMap<Unit, Integer>();
    private Stack<Unit> loopLevels = new Stack<Unit>();

    private void recordIntLocalVars() {

		Chain<Local> locals = g.getBody().getLocals();

		int count = 0;
		Iterator<Local> it = locals.iterator();
		while (it.hasNext()) {
			JimpleLocal next = (JimpleLocal) it.next();
			// String name = next.getName();
			if (next.getType() instanceof IntegerType)
				count += 1;
		}

		local_ints = new String[count];

		int i = 0;
		it = locals.iterator();
		while (it.hasNext()) {
			JimpleLocal next = (JimpleLocal) it.next();
			String name = next.getName();
			if (next.getType() instanceof IntegerType)
				local_ints[i++] = name;
		}
	}

	/* Build an environment with integer variables. */
	public void buildEnvironment() {

		recordIntLocalVars();

		String ints[] = new String[local_ints.length];

		/* add local ints */
		for (int i = 0; i < local_ints.length; i++) {
			ints[i] = local_ints[i];
		}

		env = new Environment(ints, reals);
	}

	/* Instantiate a domain. */
	private void instantiateDomain() {
		// Initialize variable 'man' to Polyhedra
        man = new Polka(true);
	}

	/* === Constructor === */
	public Analysis(UnitGraph g, SootClass jc) {
		super(g);

		this.g = g;
		this.jclass = jc;

		buildEnvironment();
		instantiateDomain();
	}

	void run() {
		doAnalysis();
	}

	// call this if you encounter statement/expression which you should not be handling
	static void unhandled(String what) {
		System.err.println("Can't handle " + what);
		System.exit(1);
	}

    // Handle conditionals.
    private void handleIf(AbstractBinopExpr expr, Abstract1 in, AWrapper ow, AWrapper ow_branchout)
            throws ApronException {

        // Make sure we handle only comparisions of integer variables.
        //if (expr.getOp1().getType().toString().equals(className)) // Is this enough??
        if (!ifSafeTypes.contains(expr.getOp1().getType().toString())) {

            ow.set(ow.get());
            ow_branchout.set(ow_branchout.get());
            return;
        }

        // Get the left and right expressions.
        Texpr1Intern lExpr = handleUnary(expr.getOp1());
        Texpr1Intern rExpr = handleUnary(expr.getOp2());

        // The branches to be set at the end.
        Abstract1 takeBranch = null;
        Abstract1 skipBranch = null;

        // The then and else expressions.
        Texpr1Node thenExpr;
        Texpr1Node elseExpr;

        // Set branches for all possibles operators.
        if (expr instanceof JLeExpr) {

            // Get then and else expressions.
            thenExpr = new Texpr1BinNode(Texpr1BinNode.OP_SUB, rExpr.toTexpr1Node(), lExpr.toTexpr1Node());
            elseExpr = new Texpr1BinNode(Texpr1BinNode.OP_SUB, lExpr.toTexpr1Node(), rExpr.toTexpr1Node());

            // Set branches.
            takeBranch = in.meetCopy(man, new Tcons1(env, Tcons1.SUP, thenExpr));
            skipBranch = in.meetCopy(man, new Tcons1(env, Tcons1.SUPEQ, elseExpr));

        } else if (expr instanceof JLtExpr) {

            // Get then and else expressions.
            thenExpr = new Texpr1BinNode(Texpr1BinNode.OP_SUB, rExpr.toTexpr1Node(), lExpr.toTexpr1Node());
            elseExpr = new Texpr1BinNode(Texpr1BinNode.OP_SUB, lExpr.toTexpr1Node(), rExpr.toTexpr1Node());

            // Set branches.
            takeBranch = in.meetCopy(man, new Tcons1(env, Tcons1.SUPEQ, thenExpr));
            skipBranch = in.meetCopy(man, new Tcons1(env, Tcons1.SUP, elseExpr));

        } else if (expr instanceof JGeExpr) {

            // Get then and else expressions.
            thenExpr = new Texpr1BinNode(Texpr1BinNode.OP_SUB, lExpr.toTexpr1Node(), rExpr.toTexpr1Node());
            elseExpr = new Texpr1BinNode(Texpr1BinNode.OP_SUB, rExpr.toTexpr1Node(), lExpr.toTexpr1Node());

            // Set branches.
            takeBranch = in.meetCopy(man, new Tcons1(env, Tcons1.SUPEQ, thenExpr));
            skipBranch = in.meetCopy(man, new Tcons1(env, Tcons1.SUP, elseExpr));

        } else if (expr instanceof JGtExpr) {

            // Get then and else expressions.
            thenExpr = new Texpr1BinNode(Texpr1BinNode.OP_SUB, lExpr.toTexpr1Node(), rExpr.toTexpr1Node());
            elseExpr = new Texpr1BinNode(Texpr1BinNode.OP_SUB, rExpr.toTexpr1Node(), lExpr.toTexpr1Node());

            // Set branches.
            takeBranch = in.meetCopy(man, new Tcons1(env, Tcons1.SUP, thenExpr));
            skipBranch = in.meetCopy(man, new Tcons1(env, Tcons1.SUPEQ, elseExpr));

        } else if (expr instanceof JEqExpr) {

            // Get then and else expressions.
            thenExpr = new Texpr1BinNode(Texpr1BinNode.OP_SUB, lExpr.toTexpr1Node(), rExpr.toTexpr1Node());
            elseExpr = new Texpr1BinNode(Texpr1BinNode.OP_SUB, rExpr.toTexpr1Node(), lExpr.toTexpr1Node());

            // Set branches (here must join the two intervals for skipBranch).
            takeBranch = in.meetCopy(man, new Tcons1(Tcons1.EQ, new Texpr1Intern(env, thenExpr)));
            Abstract1 skipBranch1 = in.meetCopy(man, new Tcons1(Tcons1.SUP, new Texpr1Intern(env, thenExpr)));
            Abstract1 skipBranch2 = in.meetCopy(man, new Tcons1(Tcons1.SUP, new Texpr1Intern(env, elseExpr)));
            skipBranch = skipBranch1.joinCopy(man, skipBranch2);

        } else if (expr instanceof JNeExpr) {

            // Get then and else expressions.
            thenExpr = new Texpr1BinNode(Texpr1BinNode.OP_SUB, rExpr.toTexpr1Node(), lExpr.toTexpr1Node());
            elseExpr = new Texpr1BinNode(Texpr1BinNode.OP_SUB, lExpr.toTexpr1Node(), rExpr.toTexpr1Node());

            // Set branches (here must join the two intervals for takeBranch).
            Abstract1 takeBranch1 = in.meetCopy(man, new Tcons1(Tcons1.SUP, new Texpr1Intern(env, thenExpr)));
            Abstract1 takeBranch2  = in.meetCopy(man, new Tcons1(Tcons1.SUP, new Texpr1Intern(env, elseExpr)));
            takeBranch = takeBranch1.joinCopy(man, takeBranch2);
            skipBranch = in.meetCopy(man, new Tcons1(Tcons1.EQ, new Texpr1Intern(env, thenExpr)));

        } else {

            unhandled("AbstractBinopExpr '" + expr.getClass().getName() + "' in handleIf()");
        }

        // Set output wrappers.
        ow.set(skipBranch);
        ow_branchout.set(takeBranch);
    }

    // handle constants
    private Texpr1Intern handleConstant(IntConstant node) {

        Texpr1Node rAr = new Texpr1CstNode(new MpqScalar(node.value));
        Texpr1Intern ret = new Texpr1Intern(env, rAr);
        return ret;
    }

    // handle locals
    private Texpr1Intern handleLocal(JimpleLocal node) {

        if (env.hasVar(node.getName())) {
            Texpr1VarNode rAr = new Texpr1VarNode(node.getName());
            return new Texpr1Intern(env, rAr);
        } else {
            return null;
        }
    }

    private Texpr1Intern handleUnary(Value node) {

        if (node instanceof IntConstant) {
            return handleConstant((IntConstant)node);
        } else if (node instanceof JimpleLocal) {
            return handleLocal((JimpleLocal)node);
        } else {
            unhandled("unary expression of type " + node.getClass().getName());
            return null;
        }
    }

	// handle assignments
	private void handleDef(Abstract1 o, Value left, Value right)
			throws ApronException {

		if (left instanceof JimpleLocal) {

			String varName = ((JimpleLocal) left).getName();

			if (right instanceof IntConstant) {
				o.assign(man, varName, handleConstant((IntConstant)right), null);
			} else if (right instanceof JimpleLocal) {
                Texpr1Intern unary = handleLocal((JimpleLocal) right);
                if (unary != null)
                    o.assign(man, varName, unary, null);
			} else if (right instanceof BinopExpr) {

                // Not sure about these two
                Texpr1Intern expLeft = handleUnary(((BinopExpr) right).getOp1());
                Texpr1Intern expRight = handleUnary(((BinopExpr) right).getOp2());

                int op = -1;

                if (right instanceof JMulExpr) {
                    op = Texpr1BinNode.OP_MUL;
                } else if (right instanceof JSubExpr) {
                    op = Texpr1BinNode.OP_SUB;
                } else if (right instanceof JAddExpr) {
                    op = Texpr1BinNode.OP_ADD;
                } else if (right instanceof JDivExpr) {
                    op = Texpr1BinNode.OP_DIV;
                } else {
                    unhandled("BinopExpr '" + right.getClass().getName() + "' in handleDef()");
                }

                Texpr1BinNode mul = new Texpr1BinNode(op, expLeft.toTexpr1Node(), expRight.toTexpr1Node());
                Texpr1Intern node = new Texpr1Intern(env, mul);
                o.assign(man, varName, node, null);

            } else if (right instanceof ThisRef) {
                //dbg("ThisRef");
            } else if (right instanceof JNewExpr) {
                //dbg("JNewExpr");
			} else {
                unhandled("assignment from " + right.getClass().getName());
			}
		} else {
            unhandled("assignment to " + left.getClass().getName());
        }
	}

	@Override
	protected void flowThrough(AWrapper current, Unit op,
			List<AWrapper> fallOut, List<AWrapper> branchOuts) {

		Stmt s = (Stmt) op;
		Abstract1 in = current.get();

		try {
            Abstract1 o = new Abstract1(man, in);
            Abstract1 o_branchout = new Abstract1(man, in);

			if (s instanceof DefinitionStmt) {
                handleDef(o,((DefinitionStmt)s).getLeftOpBox().getValue(), ((DefinitionStmt)s).getRightOpBox().getValue());
			} else if (s instanceof JIfStmt) {
                AbstractBinopExpr expr = ((AbstractBinopExpr)((JIfStmt)s).getCondition());
                AWrapper ow = new AWrapper(o);
                AWrapper ow_branchout = new AWrapper(o_branchout);
                handleIf(expr, in, ow, ow_branchout);
                o = ow.get();
                o_branchout = ow_branchout.get();
            } else if (s instanceof JGotoStmt) {
                //AWrapper ow = new AWrapper(o);
                //AWrapper ow_branchout = new AWrapper(o_branchout);
                //handleLoop(((JGotoStmt)s).getTarget(), in, ow, ow_branchout);
            } else if (s instanceof JInvokeStmt) {
                //dbg("JInvokeStmt");
                // TODO: Handle JInvokeStmt for MissileBattery API
            } else if (s instanceof JReturnVoidStmt){
                //dbg("JReturnVoidStmt");
                // TODO: Handle JReturnVoidStmt
                //... no wait, probably not...
                // Don't care!
            } else {
                unhandled(s.getClass().getName());
            }

            AWrapper ow = new AWrapper(o);
            for (AWrapper w : fallOut)
                copy(ow, w);

            AWrapper ow_branchout = new AWrapper(o_branchout);
            for (AWrapper w : branchOuts)
                copy(ow_branchout, w);

		} catch (ApronException e) {
            err(e.getMessage());
			e.printStackTrace();
		}
	}

	// Initialize starting label (top)
	@Override
	protected AWrapper entryInitialFlow() {

        AWrapper aw = null;

        try {
            // This initializes a universal element (top)
            Abstract1 el = new Abstract1(man, env);
            aw = new AWrapper(el);
            aw.man = man;
        } catch (Exception e) {
            err(e.getMessage());
        }

        return aw;
	}

	// Implement Join
	@Override
	protected void merge(AWrapper src1, AWrapper src2, AWrapper trg) {
        try {
            trg.set(Abstract1.join(man, new Abstract1[]{ src1.get(), src2.get()}));
        }
        catch (Exception e) {
            err("Error in function merge(): " + e.getMessage());
        }
	}

    // Apply widening if needed.
    @Override
    protected void merge(Unit op, AWrapper src1, AWrapper src2, AWrapper trg) {

        // Check whether Unit already registered.
        if (wideningCounts.containsKey(op)) {

            // If Unit is top of stack, proceed.
            if (loopLevels.peek().equals(op)) {

                // Compare Unit count with the defined limit.
                if (wideningCounts.get(op) <= wideningLimit) {

                    // Increment Unit counter.
                    wideningCounts.put(op, wideningCounts.get(op) + 1);

                    // Applay default merge.
                    merge(src1, src2, trg);

                } else {

                    // Apply widening.
                    try { trg.set(src1.elem.widening(man, src2.elem)); }
                    catch (ApronException ex) { ex.printStackTrace(); }
                }

            // Unit not top of stack, reset counter.
            } else {

                // Remove this Unit from both stack and counts map.
                wideningCounts.remove(loopLevels.peek());
                loopLevels.pop();

                // Applay default merge.
                merge(src1, src2, trg);
            }

        } else {

            // New Unit, register it and set its counter to 1.
            wideningCounts.put(op, 1);
            loopLevels.push(op);

            // Applay default merge.
            merge(src1, src2, trg);
        }
    }

	// Initialize all labels (bottom)
	@Override
	protected AWrapper newInitialFlow() {

        AWrapper aw = null;

        try {
            // This initializes a empty element (bottom)
            Abstract1 el = new Abstract1(man, env, true);
            aw = new AWrapper(el);
            aw.man = man;
        } catch (Exception e) {
            err(e.getMessage());
        }

        return aw;
	}

	@Override
	protected void copy(AWrapper source, AWrapper dest) {
		dest.copy(source);
	}

    private void err(String s) {
        System.out.println("[ERROR] " + s);
    }

    public static void dbg(Object s) {
        if (dbg)
            System.out.println("[DEBUG] " + s);
    }
}