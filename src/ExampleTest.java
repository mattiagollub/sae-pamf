public class ExampleTest
{
    public static void l0() // UNSAFE
    {
        MissileBattery r = new MissileBattery(6);

        int i = 20;

        r.fire(i);
    }
    /*
    public static void l1() // UNSAFE
    {
        MissileBattery r = new MissileBattery(6);

        int i = 20;
        if (i < 42)
            i = 9;
        else
            i = 4;

        r.fire(i);
    }*/

    public static void l3() // SAFE
    {
        MissileBattery r = new MissileBattery(20);

        int k = 0;
        for (int j = 0; j < 10; j++)
            k = k + 2;
        k--;

        r.fire(k);
        r.fire(k - 1);
    }

    public static void l2() // UNSAFE
    {
        MissileBattery r = new MissileBattery(6);

        int k = -2;
        for (int j = 0; j < 3; j++)
            k = k * 2;

        r.fire(k);
    }

    public static void l3() // SAFE
    {
        MissileBattery r = new MissileBattery(60);

        int k = 2;
        for (int j = 0; j < 10; j++)
            k = k + 2;

        r.fire(k);
    }

    public static void t6_safe()
    {
        MissileBattery b = new MissileBattery(6);
        int i = 5;
        b.fire(i);
        i = 0;
        b.fire(i);
    }

    public static void t7_unsafe()
    {
        MissileBattery b = new MissileBattery(6);
        int i = 5;
        b.fire(i);
        i = 0;
        b.fire(i - 1);
    }

    public static void t8_safe() {
        MissileBattery b = new MissileBattery(6);
        int i = 2;
        int j = 2;
        b.fire(i * j + 1);
        b.fire(i * j - 4);
    }

    public static void t10_safe() {
        MissileBattery b = new MissileBattery(6);
        int i = 8;
        int j = 5;
        if (i == j) {
            b.fire(8);
        }
    }

    public static void t11_safe() {
        MissileBattery b = new MissileBattery(6);
        int i = 8;
        int j = 5;
        if (i <= j) {
            b.fire(8);
        }
    }

    public static void t12_unsafe() {
        MissileBattery b = new MissileBattery(6);
        int i = 8;
        int j = 5;
        if (i >= j) {
            b.fire(8);
        }
    }

    public static void t13_safe() {
        MissileBattery b = new MissileBattery(6);

        int i = 0;
        while (i == 0) {
            i++;
            i--;
        }

        b.fire(10);
    }

    public static void t14_unsafe() {
        MissileBattery b = new MissileBattery(6);

        int i = 0;
        while (i != 0) {
            i++;
            i--;
        }

        b.fire(10);
    }

    public static void t15_safe() {
        MissileBattery b = new MissileBattery(6);
        MissileBattery c = new MissileBattery(5);
        MissileBattery a = b;

        int i = 5;

        a.fire(i);
    }

    public static void t16_safe() {
        MissileBattery b = new MissileBattery(5);
        b = new MissileBattery(6);
        MissileBattery a = b;

        int i = 5;

        a.fire(i);
    }

    public static void t17_safe() {
        MissileBattery a = new MissileBattery(6);
        MissileBattery b = new MissileBattery(6);
        MissileBattery c = new MissileBattery(6);
        int i,j;
        if (b == c) {
            i = 3;
        } else {
            i = 5;
        }

        if (b == c) {
            j = 0;
        } else {
            j = 2;
        }

        a.fire(i);
        a.fire(j);
    }

    public static void t18_safe() {
        MissileBattery a = new MissileBattery(7);
        MissileBattery b = a;
        int i = 1;
        int j = 3;
        while(i < 4) {
            i++;
            j++;
        }
        b.fire(j);
    }

    public static void t19_unsafe() {
        MissileBattery a = new MissileBattery(7);
        MissileBattery b = a;
        int i = 1;
        int j = 4;
        while(i < 4) {
            i++;
            j++;
        }
        b.fire(j);
    }

    public static void t20_unsafe() {
        MissileBattery a = new MissileBattery(9);
        MissileBattery b = a;
        int i = 1;
        int j = 0;
        while(i < 10) {
            i++;
            j++;
        }
        b.fire(j);
    }

    public static void t21_safe() {
        MissileBattery a = new MissileBattery(4);
        MissileBattery b = a;
        int i = 1;
        int j = 2;
        while(i + j < 4) {
            i++;
            j--;
        }
        b.fire(5);
    }

    public static void t22_safe() {
        MissileBattery a = new MissileBattery(16);
        MissileBattery b = a;
        int i = 2;
        int r = 1;
        int n = 4;

        r = r + i;
        i++;
        r = r + i;
        i++;
        r = r + i;
        i++;
        r = r + i;
        i++;

        b.fire(r);
    }*/
}

